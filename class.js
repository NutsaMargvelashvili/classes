class Character{
    name = '';
    role = '';

    constructor(name, role){
        this.name = name;
        this.role = role;
    }

    armor(){
        console.log(`${this.name} is ${this.role} and has ${this.armorNum()} armors`);
    }
    armorNum(){
        return Math.floor(Math.random() * 100);
      }
    damage(){
        console.log(this.role);
        (this.role === "mage" || this.role === "support")
        ? console.log("magic damage")
        : console.log("attack damage");
    }
}

class Mage extends Character{
    magicPowers = '';
    constructor(name, role, magicPowers){
        super(name, role);
        this.magicPowers = magicPowers;
      } 
      getPower(){
          console.log(`${this.name} can ${this.magicPowers}`)
      }
}

class Support extends Mage {
    num = 0;
    constructor(name, num, magicPowers) {
        super(name, 'support', magicPowers)
        this.num = num;
    }
    getPower() {
        console.log(`${this.name} is ${this.role} and he can ${this.magicPowers} ${this.num}`);
    }
}

class Adc extends Character{
    attackDamage = 0;
    constructor(attackDamage){
        super("George", "adc");
        this.attackDamage = attackDamage;
      } 
      getRange(){
          (this.attackDamage < 30)
          ? console.log("close range") 
          : console.log("wide range");
      }
}   

let nita = new Mage("Nita", "mage", "fly");
let erekle = new Support("Erekle", Math.floor(Math.random() * 100), "heal");
let nutsa = new Character("Nutsa", "assassin");
let george = new Adc(Math.floor(Math.random() * 100));
// nita.damage();
// nutsa.damage();
// erekle.damage();
// george.damage();
// nutsa.armor();
// nita.getPower();
// erekle.getPower();
// george.getRange();
